#!/bin/bash
prod="prod"
pi="pi"
target=$1
if [ "$target" == "$prod" ]
then
  # server_addr=178.124.128.109
  server_addr=cacib.by
  user_name=cacib
  deploy_path=/home/$user_name/apps
else
  if [ "$target" == "$pi" ]
  then
    server_addr=192.168.1.30
    user_name=cacib
    deploy_path=/home/$user_name/apps
  else
    server_addr=cacib.loc
    user_name=cacib
    deploy_path=/home/$user_name/apps
  fi
fi
echo "Deploy to $server_addr"
npm run build
rm cacib.stats.tar.gz
tar -zcvf cacib.stats.tar.gz build/
scp cacib.stats.tar.gz $user_name@$server_addr:$deploy_path/cacib.stats.tar.gz
scp deploy.ext.sh $user_name@$server_addr:/home/$user_name/deploy.stats.ext.sh
ssh $user_name@$server_addr