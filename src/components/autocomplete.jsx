import React from 'react';

export default class Autocomplete extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      searchKey: null,
      searchString: null,
      menuOpen: false,
      activeItem: 0,
    }
    this.filteredItems = [];
    this.children = [];
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
  }
  
  componentWillUnmount() {
    this.mounted = false;
  }

  searchKeyChanged = (event) => {
    const searchKey = event.target.value;
    this.setState({ searchKey, searchString: searchKey });
    if (this.props.onSearchKeyChanged) {
      this.props.onSearchKeyChanged(searchKey);
    }
    if (this.props.allowCreate) {
      this.optionSelected(searchKey.trim(), false);
    }
  }

  keyDown = (event) => {
    if (!this.state.menuOpen) {
      this.openMenu();
      return;
    }

    if ([13, 38, 40].indexOf(event.keyCode) > -1) {
      event.preventDefault();
    }

    switch (event.keyCode) {
      case 38:
        this.selectPrevItem();
        break;
      case 40:
        this.selectNextItem();
        break;
      case 13:
        this.chooseSelectedItem();
        break;
       default:
         return;
    }
  }

  selectPrevItem = () => {
    let activeItem = this.state.activeItem;
    activeItem -= 1;
    if (activeItem < 0) activeItem = this.filteredItems.length - 1;
    let searchKey = this.labelOf(this.filteredItems[activeItem]);
    this.setState({activeItem, searchKey});
  }

  selectNextItem = () => {
    let activeItem = this.state.activeItem;
    activeItem += 1;
    if (activeItem > this.filteredItems.length - 1) activeItem = 0;
    let searchKey = this.labelOf(this.filteredItems[activeItem]);
    this.setState({activeItem, searchKey});
  }

  chooseSelectedItem = () => {
    let activeItem = this.state.activeItem;
    this.optionSelected(this.filteredItems[activeItem]);
  }

  optionClicked = (event) => {
    // debugger;
    const value = event.target.value || event.currentTarget.value;
    const item = this.props.items.find((item) => {
      return this.valOf(item).toString() === value.toString();
    });
    this.optionSelected(item);
  }

  optionSelected = (item, close=true) => {
    if (this.props.onChange) {
      let synteticEvent = {
        target: {
          value: item ? this.valOf(item) : '',
          name: this.props.name,
          type: 'select',
        },
      };
      this.props.onChange(synteticEvent);
    }
    if (close) this.closeMenu();
  }

  clearValue = () => {
    this.optionSelected();
  }

  openMenu = () => {
    this.setState({ menuOpen: true, activeItem: 0 });
  }

  closeMenu = () => {
    this.setState({ menuOpen: false, activeItem: 0, searchKey: null });
  }

  blured = (event) => {
    let event_outside = !event.relatedTarget || ( this.children.indexOf(event.relatedTarget) < 0
          && this.children.indexOf(event.relatedTarget.offsetParent) < 0 )
    if (event_outside) {
      this.closeMenu();
    }
  }

  linkChild = (el) => {
    this.children.push(el);
  }

  labelOf = (item) => {
    if (this.props.getLabel) {
      return this.props.getLabel(item);
    } else if (this.props.labelProp) {
      return item[this.props.labelProp] || '';
    } else if (typeof item === 'string') {
      return item;
    }
    return item.label || item.title || item.name;
  }

  valOf = (item) => {
    if (this.props.getValue) {
      return this.props.getValue(item);
    } else if (this.props.valueProp) {
      return item[this.props.valueProp] || '';
    } else if (typeof item === 'string') {
      return item;
    }
    return item.value || item.id || this.labelOf(item);
  }

  keyOf = (item) => {
    let key = this.valOf(item);
    if (typeof key === 'object') {
      if (key.hasOwnProperty('id')) return key.id;
      let firstKey = Object.keys(key)[0];
      return key[firstKey];
    }
    return key;
  }

  markSubstring = (str, substr) => {
    let matcher = new RegExp(`(.*)(${substr})(.*)`, 'i');
    let [,start, mid, end] = str.match(matcher);
    return (<span>{start}<b>{mid}</b>{end}</span>);
  }

  renderItems = () => {
    const searchString = this.state.searchString || '';
    let activeItem = this.state.activeItem;
    this.filteredItems = this.props.items.filter(item => this.labelOf(item).toLowerCase().indexOf(searchString.toLowerCase()) > -1);
    // .slice(0,10);
    return this.filteredItems.map((item, idx) => {
      let className = 'dropdown-item';
      if (idx === activeItem) {
        className += ' active';
        const currentItemId = 'dropdown-item-'+this.valOf(item);
        setTimeout(() => {
          const el = document.getElementById(currentItemId);
          if (el) el.scrollIntoView(false);
        }, 10);
      }
      let label = this.markSubstring(this.labelOf(item), searchString);
      return (<button type="button" tabIndex="-1" onClick={this.optionClicked} 
        className={className} value={this.valOf(item)} 
        id={'dropdown-item-'+this.valOf(item)} key={this.keyOf(item)} >{label}</button>);
    });
  }

  render() {
    let valueLabel = '';
    if (this.props.value) {
      const item = this.props.items.find(item => this.valOf(item).toString() === this.props.value.toString() );
      if (item) {
        valueLabel = this.labelOf(item);
      } else if (this.props.allowCreate) {
        valueLabel = this.props.value;
      }
    }
    const items = ( (this.state.menuOpen) && this.renderItems() ) || [];
    const wrapperClass = 'input-group dropdown ' + (this.props.className || '');
    const menuClass = 'dropdown-menu'+ (items.length ? ' open' : '');
    const searchKey = this.state.searchKey !== null ? this.state.searchKey : valueLabel || '';
    return (
      <div className={wrapperClass} tabIndex="-1" ref={this.linkChild}>
        <input type="text" ref={this.linkChild} className="form-control" 
          onBlur={ this.blured } onKeyDown={this.keyDown} value={searchKey} 
          onClick={this.openMenu} onChange={this.searchKeyChanged} disabled={this.props.disabled}
        />
        <div id="items-dropdown" className={menuClass} ref={this.linkChild}>
          {items}
        </div>
        { this.props.clear === false || this.props.clear === 'false' ? 
          null :
          <div className="input-group-append">
            <button tabIndex="-1" className="btn btn-secondary" type="button" onClick={this.clearValue}>x</button>
          </div>
        }
      </div>
    );
  }
}
