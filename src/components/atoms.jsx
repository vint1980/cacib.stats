import React from 'react';

export function Spinner(props) {
  let size = props.size ? `${'spinner-border-'+props.size}` : 'spinner-border-sm';
  let classNames = `spinner-border ${size} ${props.className}`;
  if (props.position && props.position === 'centered') {
    return <div className="row"><div className="m-5 mx-auto">
        <span className={classNames} role="status"></span>
      </div>
    </div>
  }
  return (
    <span className={classNames} role="status"></span>
  );
}


export function SimpleSelect(props) {
  let {fieldName, optValue, optLabel} = props; 
  const className = props.className || "form-control";
  const item = props.item || {[fieldName]: ''};
  const value = props.value || item[fieldName];
  let options;
  if (optValue) {
    options = (props.list || []).map(item => (
      <option value={item[optValue]} key={item[optValue]}>{item[optLabel || optValue]}</option>
      )
    );
  } else {
    options = (props.list || []).map(item => <option value={item} key={item}>{item}</option>); 
  }
  return (
    <select id={fieldName} name={fieldName} className={className} value={value} onChange={props.onChange}>
      <option value="">{props.placeholder || 'Выберите'}</option>
      {options}
    </select>
  );
}