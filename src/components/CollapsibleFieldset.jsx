import React from 'react';
import './collapsibleFieldset.sass';

export default class CollapsibleFieldset extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: !!props.open,
    }
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
  }
  
  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (this.mounted && this.props.open !== prevProps.open) {
      this.setState({open: this.props.open});
    }
  }

  toggleOpen = () => {
    this.setState({open: !this.state.open});
  }

  render() {
    const className = (this.props.className || '') + ' ' + (this.state.open ? 'open' : 'closed');
    return (
      <fieldset id={this.props.id || null} className={className}>
        <legend onClick={this.toggleOpen}>{this.props.title || '---'}</legend>
        {this.props.children}
      </fieldset>
    );
  }
}