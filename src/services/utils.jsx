// import React from 'react';
import moment from 'moment';
import ruLocale from 'moment/locale/ru.js';

moment.updateLocale('ru', ruLocale);


export function formatDate(date, format='LL') {
  if (moment(date)) {
    return moment(date).format(format);
  }
  return '--';
}

export function translateTo(lang='ru', allTranslations={}, showError=true) {
  const translations = allTranslations[lang] || {}; 
  return function(key, defaultTranslation) {
    return translations[key] || (defaultTranslation ? defaultTranslation : (showError ? 'No translation for ' + key : key) )
  }
}

export function handleErrors(request) {
  let errors;
  if (request.response) {
    errors = (request.response.data && request.response.data.errors) || [request.response.statusText];
  } else if (request.message) {
    errors = [request.message];
  } else {
    errors = ["Unknown network error."];
  }
  return errors;
}
