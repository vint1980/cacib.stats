
export const showClasses = [
  { id: '', name_short: 'N',     name_short_ru: 'N',     name: 'Выберите класс', name_lat: 'Select class' },
  { id: 1,  name_short: 'Baby',  name_short_ru: 'бэби',  name: 'Класс бэби', name_lat: 'Baby class' },
  { id: 2,  name_short: 'Puppy', name_short_ru: 'щенки', name: 'Класс щенков', name_lat: 'Puppy class' },
  { id: 3,  name_short: 'Jun',   name_short_ru: 'юниоры',   name: 'Класс Юниоров', name_lat: 'Junior class' },
  { id: 4,  name_short: 'Int',   name_short_ru: 'промежут.',   name: 'Промежуточный класс', name_lat: 'Intermediate class' },
  { id: 5,  name_short: 'Open',  name_short_ru: 'открытый',  name: 'Открытый класс', name_lat: 'Open class' },
  { id: 6,  name_short: 'Work',  name_short_ru: 'рабочий',  name: 'Рабочий класс', name_lat: 'Working class' },
  { id: 7,  name_short: 'Champ', name_short_ru: 'чемпионы', name: 'Класс чемпионов', name_lat: 'Champion class' },
  { id: 8,  name_short: 'Vet',   name_short_ru: 'ветераны',   name: 'Класс ветеранов', name_lat: 'Veteran class' },
]
