
export const translations = {
  ru: {
    registrationStatsHeader: 'Статистика регистраций на выставку.',
    toSeeChooseShow: 'Для просмотра, выберите выставку',
    nowChooseBreed: 'Теперь выберите породу',
    regStatsOfBreed: 'Статистика регистраций собак породы',
    forTheShow: 'на выставку',
    showedOnlyLast4Chars: 'Показаны только последние 4 символа родословной собаки.',
    males: 'Кобели',
    females: 'Суки',
    noDogsRegistered: 'Пока не зарегистрировано ни одной собаки выбранной породы.',
  },
  en: {
    registrationStatsHeader: 'Show registration statistics.',
    toSeeChooseShow: 'Please choose show to see stats by',
    nowChooseBreed: 'Now please choose breed',
    regStatsOfBreed: 'Dogs registration stats for breed',
    forTheShow: 'to the show',
    showedOnlyLast4Chars: 'Only 4 last chars of a dog\'s pedigree showed',
    males: 'Males',
    females: 'Females',
    noDogsRegistered: 'There are no dogs of the breed registered yet.',
  },
}