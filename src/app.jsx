import React from 'react';
import './app.sass';
import axios from 'axios';
import { handleErrors, formatDate, translateTo } from 'services/utils'
import { translations } from 'services/translations';
import { showClasses } from 'services/constants'
import { Spinner } from 'components/atoms';
import Autocomplete from 'components/autocomplete';
import CollapsibleFieldset from 'components/CollapsibleFieldset';

const isDevMode = process.env.NODE_ENV === 'development';
const serverUrl = 'http://' + (isDevMode ? 'localhost:3000' : '86.57.153.2') + '/vapi/';
const showsUrl = serverUrl + 'shows';
const breedsUrl = serverUrl + 'breeds';
const defaultLang = 'ru';

let translator = translateTo(defaultLang, translations);

export default class StatsApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shows: [],
      breeds: [],
      showId: '',
      breedId: '',
      show: null,
      breed: null,
      errors: [],
      loading: false,
      lang: defaultLang,
    }
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    this.loadShows();
    this.loadBreeds();
  }
  
  componentWillUnmount() {
    this.mounted = false;
  }

  loadShows() {
    this.setState({ loading: true });
    axios.get(showsUrl).then((response) => {
      if (this.mounted) {
        const shows = response.data || [];
        this.setState({ shows });
      }
    }).catch(this.handleErrors).then(() => {
      if (this.mounted) {
        this.setState({loading: false})
      }
    });
  }

  loadBreeds() {
    axios.get(breedsUrl).then((response) => {
      if (this.mounted) {
        const breeds = response.data || [];
        this.setState({ breeds });
      }
    }).catch(this.handleErrors);
  }

  loadStats(showId, breedId) {
    this.setState({ loading: true });
    axios.get(`${showsUrl}/${showId}/stats?breed_id=${breedId}`).then((response) => {
      if (this.mounted) {
        const data = response.data || {};
        let breed = data.breed || {};
        const show = data.show || {};
        breed.dogs = data.dogs || [];
        this.setState({ show, breed });
      }
    }).catch(request => {
      if (this.mounted) {
        this.setState({ breed: null });
        this.handleErrors(request);
      }
    }).then(() => {
      if (this.mounted) {
        this.setState({loading: false})
      }
    });
  }

  handleErrors = (request) => {
    const errors = handleErrors(request);
    this.setState({ errors });
  }

  onShowChosen = ({target}) => {
    const showId = parseInt(target.id.replace(/\D/g,''));
    if (showId) {
      this.setState({ showId });
      if (this.state.breedId) {
        this.loadStats(showId, this.state.breedId)
      }
    }
  }

  onBreedChosen = ({target}) => {
    // debugger;
    const breedId = parseInt(target.value);
    if (breedId) {
      this.setState({ breedId });
      this.loadStats(this.state.showId, breedId);
    }
  }

  switchLang = ({target}) => {
    if (['ru','en'].includes(target.value)) {
      const lang = target.value;
      this.setState({ lang });
      translator = translateTo(lang, translations);
    }
  }

  render() {
    let errors = null;
    const t = translator;
    if (this.state.errors) {
      errors = (
        <ul className="errors">
          {this.state.errors.map(err => <li>{err}</li>)}
        </ul>
      );
    }
    let shows = null;
    if (this.state.shows && this.state.shows.length) {
      shows = (
        <div>
          <p>{t('toSeeChooseShow')}:</p>
          <ol>
            {this.state.shows.map(show => {
              const btnClassName = 'btn-link'+(this.state.showId === show.id ? ' current' : '');
              return (
                <li key={show.id}>
                  <button id={`show-${show.id}`} onClick={this.onShowChosen} className={btnClassName} >
                    {show.name}
                  </button>
                </li>);
              })
            }
          </ol>
        </div>
      );
    }
    let breeds = null;
    if (this.state.showId && this.state.breeds && this.state.breeds.length) {
      const labelProp = this.state.lang === 'ru' ? 'name' : 'name_lat';
      breeds = (
        <div>
          <p>{t('nowChooseBreed')}</p>
          <Autocomplete items={this.state.breeds} name="breedId" value={this.state.breedId} 
            valueProp="id" clear="false" labelProp={labelProp} onChange={this.onBreedChosen} />
        </div>
      )
    }
    return (
      <div className="container">
        <div className="langs">
          <button className="btn btn-link" onClick={this.switchLang} value="ru">Рус</button>
          <button className="btn btn-link" onClick={this.switchLang} value="en">Eng</button>
        </div>
        <h2>{t('registrationStatsHeader')}</h2>      
        {this.state.loading ? <Spinner /> : null}
        {errors}
        {shows}
        {breeds}
        { this.state.breed ? <DogStats show={this.state.show} breed={this.state.breed} lang={this.state.lang} /> : null}
      </div>
    );
  }

}

function DogStats({show, breed, lang}) {
  const t = translator;
  if (lang === 'en') {
    return (
      <div className="show-breed-stats">
        <h4>{t('regStatsOfBreed')} 
          <strong>{breed.name_lat}</strong> {t('forTheShow')}  
          <strong>{show.name} at {formatDate(show.starts_at)}</strong></h4>
          <p className="note">{t('showedOnlyLast4Chars')}</p>
        <DogsByBreed dogs={breed.dogs} open={true} lang={lang}/>
      </div>
    )
  }
  return (
    <div className="show-breed-stats">
      <h4>{t('regStatsOfBreed')} 
        <strong>{breed.name}</strong> {t('forTheShow')} 
        <strong>{show.name} от {formatDate(show.starts_at)}</strong></h4>
        <p className="note">{t('showedOnlyLast4Chars')}</p>
      <DogsByBreed dogs={breed.dogs} open={true} lang={lang}/>
    </div>
  );
}


function DogsByBreed({dogs, open, lang}) {
  const t = translator;
  const females = dogs.filter(dog => dog.sex === 'female');
  const males = dogs.filter(dog => dog.sex === 'male');
  return (
    <div className="p-3">
      {males.length ? (
        <CollapsibleFieldset open={open} title={t('males') + ' (' + males.length + ')'}>
          <DogsByClasses dogs={males} open={open} lang={lang}/>
        </CollapsibleFieldset>
        ) : null
      }
      {females.length ? (
        <CollapsibleFieldset open={open} title={t('females') + ' (' + females.length + ')'}>
          <DogsByClasses dogs={females} open={open} lang={lang} />
        </CollapsibleFieldset>
        ) : null
      }
      {males.length + females.length ? null : <p>{t('noDogsRegistered')}</p>}
    </div>
  );
}

function DogsByClasses({dogs, open, lang}) {
  return showClasses.map(showClass => {
    if (!showClass.id) return null;
    const classDogs = dogs.filter(dog => dog.class_in_show === showClass.id);
    if (!classDogs.length) return null;
    return (
      <CollapsibleFieldset 
        open={open}
        className="m-2"
        key={showClass.id} 
        title={lang === 'en' ? showClass.name_lat : showClass.name + ' (' + classDogs.length + ')'}>
        {classDogs.map(dog => '...'+dog.pedigree).join(', ')}
      </CollapsibleFieldset>
    );
  }).filter(item => item);
}